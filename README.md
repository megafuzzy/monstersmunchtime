# Project description

Megafuzzy Monsters Munchtime is a fun minigame for babies, toddlers and preschoolers. Those Monsters will eat just about anything.

- 3 different monsters with unique sound effects
- Dozens of different items which appear randomly, making each game a new experience for your kid.
- Unique reaction by each monster for each item

**This application develops:**

- Motor skills
- Reactions
- Social interaction
- Empathy
- Identifying with feelings and expressions

**Please note, this project has not been migrated to 64-bit and therefore is deprecated and not available in Play store anymore. This repository is here only for the job hunting purposes**

# My role in development and technologies used

* The game was developed solely by myself, using Unity3D and scripting with C#. 
* All credits for the graphics go to https://carolinamorera.com/

**Please check the demo reel for all MegaFuzzy Games in Screenshots folder**