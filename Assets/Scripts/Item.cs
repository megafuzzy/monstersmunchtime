﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public Vector2 originalPosition;
    public enum ItemSize { Small, Medium, Large };
  
    public ItemSize itemSize;

    public void setOriginalPosition(Vector2 pos)
    {
        originalPosition = pos;
    }

    public void getOriginalPosition(Vector2 oPosArg)
    {
        oPosArg = originalPosition;
    }

}
