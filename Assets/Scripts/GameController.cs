﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;


/* MONSTER MUNCHTIME by Megafuzzy Interactive 2017
 */

public class GameController : MonoBehaviour
{
    //private bool mouseControl = false;
    //private bool draggingItem = false;
    //private GameObject draggedObject; //change this to array?
    //private GameObject[] items;
    
    //private Vector3 originalPosition;
    private Animator BigMonsterAnimator = null;
    private Animator MiddleMonsterAnimator = null;
    private Animator LittleMonsterAnimator = null;
    private Animator RadioAnimator = null;
    private Animator NotesAnimator = null;

    private GameObject bigMonster = null;
    private GameObject littleMonster = null;
    private GameObject middleMonster = null;
    private GameObject radio = null;
    private GameObject notes = null;
    private GameObject exitButton = null;

    private AudioSource[] bigMonsterSounds = null; //Array will hold sounds in these indexes: 0) Eating 1) Pleased 2) Disgusted 3) Poked 
    private AudioSource[] middleMonsterSounds = null;
    private AudioSource[] littleMonsterSounds = null;
    private AudioSource[] radioSounds = null;

    private float gameVolumeRadio = 0.4f;
    private float menuVolumeRadio = 0.2f;

    public float timeBetweenPokes = 0.3333f; //these are needed to prevent multiple pokes to radio
    private float timeWhenRadioPokedLastTime;

    private float timeWhenFinalDanceStarted;
    private float timeUntilFinalDanceCanStop = 5.0f;
    private float timeUntilFinalDanceStopsAutomatically = 14.0f;

    private int itemCounter = 10;
    private bool radioPlaying = true;
    private bool finalDancePlaying = false;
    //private bool gameMode = false;

    private Touch[] myTouches;
    //private bool[] fingerDragging = new bool[10] { false, false, false, false, false, false, false, false, false, false };
    private Item[] itemDraggedByFinger = new Item[10]; //index will match finger id

    InterstitialAd interstitial;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Start");
        generateMonsters(); //Including radio and notes
        //exitButton = GameObject.FindGameObjectWithTag("ExitButton");
        //exitButton.SetActive(false); //to hide it from canvas
        placeItemsByRandom();

#if UNITY_ANDROID
        string appId = "ca-app-pub-8347667331834511~4259700091";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
#else
            string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8347667331834511/5867175673"; //my real ad unit for feed the monsters 
        //string adUnitId = "ca-app-pub-3940256099942544/1033173712"; //google sample ad unit for interstitial ads
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        Debug.Log("adUnit created: " + interstitial.ToString());
        Debug.Log("AdRequest built: " + request.ToString());
    }

    void Update()
    {

        if (finalDancePlaying)
        {
            if (Time.time >= timeWhenFinalDanceStarted + timeUntilFinalDanceStopsAutomatically)
            {
                ////Debug.Log("Scene will now reset automatically");
                restartGame();
                //SceneManager.LoadScene("FeedTheMonsters");
                //return automatically to main menu and reset scene

            }
            else if (Time.time > timeWhenFinalDanceStarted + timeUntilFinalDanceCanStop && Time.time < timeWhenFinalDanceStarted + timeUntilFinalDanceStopsAutomatically)
            {
                ////Debug.Log("Scene can now be clicked again");
                //enable clicking on the view
                if (HasInput)
                {
                    ////Debug.Log("Input caught");
                    //MenuController.hasPlayedInThisSession = true;
                    restartGame();
                    //SceneManager.LoadScene("FeedTheMonsters");
                }
            }

        }
        else
        {
            updateTouch();                                   
        }
    }


    private void updateTouch()
    {
        myTouches = Input.touches;

        foreach (Touch t in myTouches) //loop through each finger
        {
            ////Debug.Log("touch detected: " + t.fingerId);
            var inputPosition = currentTouchPosition(t.position);
            var phase = t.phase;
            var fingerId = t.fingerId;

            switch (phase)
            {
                case TouchPhase.Began: //used only for pokes

                    RaycastHit2D[] monsterHits = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
                    showPosition(inputPosition);
                    if (monsterHits.Length == 1) //only one object is hit
                    {
                        var hit = monsterHits[0];
                        GameObject hitObject = hit.transform.gameObject;
                        if (isMonster(hitObject)) //the object which was hit was an item and the same finger is not dragging anything at the moment
                        {
                            pokeMonster(hitObject);
                        }
                    }
                    break;

                case TouchPhase.Moved:

                    //if dragging is on, don't throw rays
                    //check what gameObject is being dragged by this finger

                    Item itemDraggedByThisFinger = itemDraggedByFinger[fingerId];
                    if (itemDraggedByThisFinger != null)
                    {
                        //redraw position
                        itemDraggedByThisFinger.transform.position = inputPosition;
                    }
                    else
                    {
                        //check if finger is hitting any items to be dragged
                        RaycastHit2D[] itemHits = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
                        for (int i = 0; i < itemHits.Length; i++) //loop through each collider which are hit by this finger in this frame
                        {
                            ////Debug.Log("i is " + i);
                            var hit = itemHits[i]; //returns for example hits to item collider in index 0 and monster collider in index 1
                            if (hit.transform != null) //hit usually always has transform but good to check
                            {
                                GameObject hitObject = hit.transform.gameObject;

                                if (!isMonster(hitObject))
                                {
                                    //Debug.Log("New dragging began for " + hitObject.tag);
                                    Item item = hitObject.GetComponent<Item>();
                                    itemDraggedByFinger[fingerId] = item;
                                    hitObject.GetComponent<SpriteRenderer>().sortingOrder++;
                                }
                            }
                        }                     
                    }
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Ended:
                    ////Debug.Log("Ended: " + hitObject.tag);
                    Item itemDroppedByThisFinger = itemDraggedByFinger[fingerId];

                    if (itemDroppedByThisFinger != null)
                    {
                        ////Debug.Log("Item " + itemDroppedByThisFinger.gameObject.tag + " was dropped by finger " + fingerId);
                        GameObject hitObject = itemDroppedByThisFinger.gameObject; // GameObject.FindGameObjectWithTag(itemDroppedByThisFinger.tag);
                        Vector2 dropPosition = inputPosition;

                        RaycastHit2D[] dropColliders = Physics2D.RaycastAll(dropPosition, dropPosition, 0.5f); //returns all colliders the drop position hits
                        itemDraggedByFinger[fingerId] = null;
                        bool monsterFound = false;
                        ////Debug.Log("DropColliders has " + dropColliders.Length + " elements in it");
                        if (dropColliders.Length > 0) //If length is 1 the item is dropping to empty position, if its 2 or more there is either monster or other item below it
                        {

                            for (int counter = 0; counter < dropColliders.Length; counter++)
                            {
                                ////Debug.Log("Collider at index " + counter);
                                var colliderHit = dropColliders[counter];
                                GameObject dropObject = colliderHit.transform.gameObject;
                                ////Debug.Log("Collider at index " + counter + " is " + dropObject.tag);
                                if (isMonster(dropObject) && dropObject.tag != "Radio") //radio cannot eat so don't even try it
                                {
                                    ////Debug.Log("Monster was found! NOw eat!");
                                    monsterFound = true;
                                    monsterEat(dropObject, hitObject); //dropObject is monster and hitObject is item
                                    //break;
                                }

                            }

                            if (!monsterFound)
                            {
                                dropBackToOrigPos(hitObject);
                            }


                            hitObject.GetComponent<SpriteRenderer>().sortingOrder--;
                        }
                    }
                    else
                    {
                        //no item was carried in this drag event, no need to do anything
                    }


                    break;
                case TouchPhase.Canceled:
                    break;

            }

            RaycastHit2D[] colliderHits = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
            showPosition(inputPosition);

            for (int i = 0; i < colliderHits.Length; i++) //loop through each collider which are hit by this finger in this frame
            {
                ////Debug.Log("i is " + i);
                var hit = colliderHits[i]; //returns for example hits to item collider in index 0 and monster collider in index 1

                

                if (hit.transform != null) //hit usually always has transform but good to check
                {
                    GameObject hitObject = hit.transform.gameObject;

                     //if 
                }                           
            }
        }
    }
      

    private void placeItemsByRandom()
    {
        //add all items to array, one array per item size category

        GameObject[] smallItems = {
            GameObject.FindWithTag("Apple"),
            GameObject.FindWithTag("Cupcake"),
            GameObject.FindWithTag("Keys"),
            GameObject.FindWithTag("Pacifier"),
            GameObject.FindWithTag("Shoe"),
            GameObject.FindWithTag("Lemon")
        };

        GameObject[] mediumItems = {
            GameObject.FindWithTag("Eggplant"),
            GameObject.FindWithTag("Carrot"),
            GameObject.FindWithTag("Burger"),
            GameObject.FindWithTag("Glasses"),
            GameObject.FindWithTag("CarToy"),
            GameObject.FindWithTag("Bananas"),
            GameObject.FindWithTag("Broccoli"),
            GameObject.FindWithTag("Cellphone"),
            GameObject.FindWithTag("BabyBottle"),
            GameObject.FindWithTag("Cake"),
            GameObject.FindWithTag("ChocolateBar"),
            GameObject.FindWithTag("CoffeeMug"),
            GameObject.FindWithTag("Donut"),
            GameObject.FindWithTag("Mitten"),
            GameObject.FindWithTag("Sandwich")
        };

        GameObject[] largeItems = {
            GameObject.FindWithTag("Hat"),
            GameObject.FindWithTag("Ball"),
            GameObject.FindWithTag("Book"),
            GameObject.FindWithTag("Guitar"),
            GameObject.FindWithTag("SoccerBall"),
            GameObject.FindWithTag("ToyTrain")

        };

        //shuffle arrays
        Shuffle<GameObject>(smallItems);
        Shuffle<GameObject>(mediumItems);
        Shuffle<GameObject>(largeItems);


        //place 2 items to large positions

        ////Debug.Log("Now placing 2 large items"); 
        placeAndStorePosition(largeItems[0], new Vector2(-7.9f, -2.6f));
        placeAndStorePosition(largeItems[1], new Vector2(7.3f, -4.9f));
        Destroy(largeItems[2]);
        Destroy(largeItems[3]);
        Destroy(largeItems[4]);
        Destroy(largeItems[5]);

        ////Debug.Log("Now placing 6 medium items");
        placeAndStorePosition(mediumItems[0], new Vector2(-7.7f, -5.2f));
        placeAndStorePosition(mediumItems[1], new Vector2(-4.5f, -5.3f));
        placeAndStorePosition(mediumItems[2], new Vector2(-2.5f, -2.5f));
        placeAndStorePosition(mediumItems[3], new Vector2(1.2f, -3.0f));
        placeAndStorePosition(mediumItems[4], new Vector2(1.3f, -5.3f));
        placeAndStorePosition(mediumItems[5], new Vector2(3.9f, -4.3f));
        Destroy(mediumItems[6]);
        Destroy(mediumItems[7]);
        Destroy(mediumItems[8]);
        Destroy(mediumItems[9]);
        Destroy(mediumItems[10]);
        Destroy(mediumItems[11]);
        Destroy(mediumItems[12]);
        Destroy(mediumItems[13]);
        Destroy(mediumItems[14]);



        ////Debug.Log("Now placing 2 small items");
        placeAndStorePosition(smallItems[0], new Vector2(-5.4f, -3.3f));
        placeAndStorePosition(smallItems[1], new Vector2(-1.7f, -4.7f));
        Destroy(smallItems[2]);
        Destroy(smallItems[3]);
        Destroy(smallItems[4]);
        Destroy(smallItems[5]);
    }

    private void placeAndStorePosition(GameObject obj, Vector2 pos)
    {
        obj.transform.position = pos;
        obj.SendMessage("setOriginalPosition", pos);
    }
        
    static void Shuffle<GameObject>(GameObject[] array)
    {
        int n = array.Length;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(Random.Range(0f, 1f * (n - i)));
            GameObject goArray = array[r];
            array[r] = array[i];
            array[i] = goArray;
        }
    }

    private void LateUpdate()
    {
        if (itemCounter < 1 && !finalDancePlaying) StartCoroutine(playFinalShow());
    }

    private void generateMonsters()
    {
        bigMonster = GameObject.FindWithTag("BigMonster");
        middleMonster = GameObject.FindWithTag("MiddleMonster");
        littleMonster = GameObject.FindWithTag("LittleMonster");
        radio = GameObject.FindWithTag("Radio");
        notes = GameObject.FindWithTag("Notes");
        exitButton = GameObject.FindWithTag("ExitButton");
        if (BigMonsterAnimator == null) BigMonsterAnimator = bigMonster.GetComponent<Animator>();
        //BigMonsterAnimator.speed = 0.0f;
        if (MiddleMonsterAnimator == null) MiddleMonsterAnimator = middleMonster.GetComponent<Animator>();
        //MiddleMonsterAnimator.speed = 0.0f;
        if (LittleMonsterAnimator == null) LittleMonsterAnimator = littleMonster.GetComponent<Animator>();
        //LittleMonsterAnimator.speed = 0.0f;
        if (RadioAnimator == null) RadioAnimator = radio.GetComponent<Animator>();
        //RadioAnimator.speed = 0.0f;
        if (NotesAnimator == null) NotesAnimator = notes.GetComponent<Animator>();
        //NotesAnimator.speed = 0.0f;

        if (littleMonsterSounds == null) littleMonsterSounds = littleMonster.GetComponents<AudioSource>();
        if (middleMonsterSounds == null) middleMonsterSounds = middleMonster.GetComponents<AudioSource>();
        if (bigMonsterSounds == null) bigMonsterSounds = bigMonster.GetComponents<AudioSource>();
        if (radioSounds == null) radioSounds = radio.GetComponents<AudioSource>();

        //RadioAnimator.StartPlayback();
        radioSounds[0].Play(); //this is needed if play on awake is off
        gameModeOff(); //until play button is clicked

    }

    

    private Vector2 currentTouchPosition(Vector2 pos)
    {

        //var touchPos = Input.GetTouch(0).position; 
            //var mousePos = Input.mousePosition;
            Vector3 positionXYZ = new Vector3(pos.x, pos.y, 14.0f); // select distance = 10 units from the camera

            Vector2 inputPos;
            inputPos = Camera.main.ScreenToWorldPoint(positionXYZ);
            
            return inputPos;
        
    }



    //private void DragOrPickUp()
    //{
    //    //Vector2 inputPosition = currentTouchPosition();
    //    Vector3 inputPosition = Input.mousePosition;
    //    Vector3 mousePos = Input.mousePosition;
    //    Vector3 positionXYZ = new Vector3(mousePos.x, mousePos.y, 14.0f); // select distance = 10 units from the camera

    //    Vector2 posForRaycasting;
    //    posForRaycasting = Camera.main.ScreenToWorldPoint(positionXYZ);

    //    if (draggingItem) //updates draggable items 
    //    {
    //        Vector3 touchOffset;
            
    //        touchOffset = draggedObject.transform.position - inputPosition;
    //        draggedObject.transform.position = inputPosition + touchOffset;
    //        //Debug.Log("Dragging " + draggedObject.tag + " at pos " + draggedObject.transform.position);
    //    }
    //    else
    //    {
    //        ////Debug.Log("Pickup, casting rays with world position " + inputPosition.ToString());
    //        RaycastHit2D[] touches = Physics2D.RaycastAll(posForRaycasting, posForRaycasting, 0.5f);
    //        //Debug.Log("Touches: " + touches.ToString());


    //        for (int i = 0; i < touches.Length; i++)
    //        {
    //            //Do something with the touches

    //            ////Debug.Log("Touches length more than 0");
    //            var hit = touches[i]; //take first hit from the touches array
    //                                  //this array needs to be looped though when multitouch support is implemented
    //            GameObject hitObject = hit.transform.gameObject;
    //            if (hit.transform != null)
    //            {
    //                //Debug.Log("Touch found at " + transform.position);
    //                if (!isMonster(hitObject)) //the object which was hit was an item
    //                {
    //                    //Debug.Log("Object " + hitObject.tag+ " hit, now trying to send message");
    //                    //store original position of the item
                        
    //                    hitObject.SendMessage("setOriginalPosition", hitObject.transform.position);
    //                    //storeOriginalPosition(hitObject.GetComponent<>(), hitObject.transform.position);
    //                    ////Debug.Log("hit transform is not null so dragging now");
    //                    draggingItem = true;
    //                    draggedObject = hitObject;
    //                    //Debug.Log("Found dragged object: " + draggedObject.ToString());
    //                    Vector3 touchOffset = (Vector3)hit.transform.position - inputPosition;
    //                    hitObject.transform.position = inputPosition + touchOffset;
    //                    //Debug.Log("Object should move this much on x axis: " + touchOffset.x + " and this much on y: " + touchOffset.y);
    //                    //draggedObject.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
    //                    hitObject.GetComponent<SpriteRenderer>().sortingOrder++;
    //                }
    //                else //the object which was hit was a monster
    //                {
    //                    pokeMonster(hitObject);
    //                }
    //            }

    //        }
    //    }
    //}

    private void storeOriginalPosition(Item item, Vector3 itemPosition)
    {
        //item.originalPosition = itemPosition;
    }

    private bool isMonster(GameObject hitObject)
    {
        if (hitObject.tag == "BigMonster" || hitObject.tag == "MiddleMonster" || hitObject.tag == "LittleMonster" || hitObject.tag == "Radio") //radio and exit button are assumed a monster as they should not be dragged
        {
            return true; //is monster
        }
        else return false; //is something else than a monster
    }

    private void pokeMonster(GameObject monsterObject)
    {
        switch (monsterObject.tag)
        {
            case "BigMonster":
                
                if (BigMonsterAnimator == null) BigMonsterAnimator = monsterObject.GetComponent<Animator>();

                BigMonsterAnimator.SetTrigger("Poke");
                bigMonsterSounds[3].Play();
                break;


            case "MiddleMonster":
                
                if (MiddleMonsterAnimator == null) MiddleMonsterAnimator = monsterObject.GetComponent<Animator>();
                MiddleMonsterAnimator.SetTrigger("Poke");
                middleMonsterSounds[3].Play();
                break;

            case "LittleMonster":
                
                if (LittleMonsterAnimator == null) LittleMonsterAnimator = monsterObject.GetComponent<Animator>();
                LittleMonsterAnimator.SetTrigger("Poke");
                littleMonsterSounds[3].Play();


                break;

            case "Radio":
                ////Debug.Log("Radio poked");
                if (Time.time >= timeWhenRadioPokedLastTime)
                {

                    

                    if (RadioAnimator == null) RadioAnimator = monsterObject.GetComponent<Animator>();
                    //AudioSource audio = radio.GetComponent<AudioSource>();
                    if (!radioPlaying)
                    {
                        radioSounds[0].mute = false;
                        
                        RadioAnimator.StopPlayback(); //sets animation back to idle
                        //NotesAnimator.enabled = true;
                        notes.SetActive(true);
                        radioPlaying = true;
                    }
                    else
                    {
                        radioSounds[0].mute = true;
                        //NotesAnimator.enabled = false;
                        notes.SetActive(false);
                        ////Debug.Log("Stopping music");
                        RadioAnimator.StartPlayback(); //as idle animation is already playing by default start actually stops it?
                        radioPlaying = false;
                    }
                    timeWhenRadioPokedLastTime = Time.time + timeBetweenPokes;
                }
                break;
            case "ExitButton":
                //Debug.Log("Exit button hit!");
                //if (LittleMonsterAnimator == null) LittleMonsterAnimator = monsterObject.GetComponent<Animator>();
                //LittleMonsterAnimator.SetTrigger("Poke");
                //littleMonsterSounds[3].Play();
                restartGame();


               

                break;
            default:
                //some other monster for future reference
                break;
        }
    }


    private bool HasInput //just to check that something has been touched on the screen
    {
        get
        {
            ////Debug.Log(gameObject.ToString() + " has input");
            // returns true if either the mouse button is down or at least one touch is felt on the screen
            return Input.GetMouseButton(0);
            //return Input.touchCount > 0; //for dedicated touch controls

        }
    }

    //void DropItem()
    //{
    //    ////Debug.Log("Drop event");
    //    draggingItem = false;
    //    //draggedObject.transform.localScale = new Vector3(1f, 1f, 1f);
    //    draggedObject.GetComponent<SpriteRenderer>().sortingOrder--;
    //    Vector2 dropPosition = draggedObject.transform.position;
    //    //showPosition(dropPosition);
    //    handleDrop(dropPosition);
    //}

    void showPosition(Vector2 pos)
    {
        //GameObject pointer = GameObject.FindGameObjectWithTag("TestPointer");
        //if (pointer != null) pointer.transform.position = pos;
    }

    /*
     * In this function game detects whether drop position is on
     * any of the 3 monsters mouth area. If it is, it will trigger
     * the corresponding animation based on the monster and the item.
    // */
    //void handleDrop(Vector2 dropPosition)
    //{

    //    RaycastHit2D[] touches = Physics2D.RaycastAll(dropPosition, dropPosition, 0.5f); //returns all colliders the drop position hits
    //    ////Debug.Log("Ray hit this many colliders on drop: "+touches.Length);

    //    bool monsterFound = false;

    //    if (touches.Length > 0) //check if there is any collider under the draggable object, it will be in array position 1 if there is
    //    {
            
    //        var i = 0;
    //        while (i < touches.Length)
    //        {
    //            var hit = touches[i];
    //            GameObject dropObject = hit.transform.gameObject;
    //            if (isMonster(dropObject))
    //            {
    //                monsterFound = true;
    //                monsterEat(dropObject, draggedObject);
    //                break;
    //            }
                
    //            i++;
    //        }

    //    }

    //    if (!monsterFound)
    //    {
    //        dropBackToOrigPos(draggedObject);
    //    }


    //        //dropObject.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);


    //    //}
    //    //else dropBackToOrigPos(draggedObject);
    //}

    private void dropBackToOrigPos(GameObject obj)
    {
        Item itemScript = obj.GetComponent<Item>();

        //int fingerIdOnItem = itemScript.fingerId;
        ////Debug.Log("Item missed monster collider at position: " + obj.transform.position);
        Vector2 origPos = itemScript.originalPosition;
        
        ////Debug.Log("Items original position is supposed to be " + origPos.ToString());
        obj.transform.position = origPos;
        //obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }




    private void monsterEat(GameObject monsterObject, GameObject itemObject)
    {

        
        switch (monsterObject.tag)
        {
            case "BigMonster":
                monsterReactsToItem(monsterObject, itemObject);
                Destroy(itemObject);   
                break;

            case "MiddleMonster":
                monsterReactsToItem(monsterObject, itemObject);
                Destroy(itemObject);
                break;

            case "LittleMonster":
                monsterReactsToItem(monsterObject, itemObject);
                Destroy(itemObject);
                
                break;
            case "Radio":
                break;
            case "ExitButton":
                break;

            default:
                //some other monster for future reference
                break;
        }
        itemCounter--;
        
    }

    public void gameModeOn()
    {
        //gameMode = true;
        if (radioSounds != null) radioSounds[0].volume = gameVolumeRadio;
        BigMonsterAnimator.speed = 1.0f;
        MiddleMonsterAnimator.speed = 1.0f;
        LittleMonsterAnimator.speed = 1.0f;
        RadioAnimator.speed = 1.0f;
        NotesAnimator.speed = 1.0f;

        //GameObject exitButton = GameObject.FindGameObjectWithTag("ExitButton");
        if (exitButton != null) exitButton.SetActive(true);
    }

    public void gameModeOff()
    {
        //gameMode = false;
        if (radioSounds != null) radioSounds[0].volume = menuVolumeRadio;
        BigMonsterAnimator.speed = 0.0f;
        MiddleMonsterAnimator.speed = 0.0f;
        LittleMonsterAnimator.speed = 0.0f;
        RadioAnimator.speed = 0.0f;
        NotesAnimator.speed = 0.0f;
        if (exitButton != null) exitButton.SetActive(false);

    }

    public void silenceRadio()
    {
        if (radioSounds != null) radioSounds[0].volume = 0.0f;
        //if (radioSounds != null) radioSounds[1].volume = 0.0f;

    }

    public void openRadio()
    {
        if (radioSounds != null) radioSounds[0].volume = 0.2f;
        radioSounds[0].Play();
        //if (radioSounds != null) radioSounds[1].volume = originalVolumeDance;

    }

    public void restartGame()
    {
        Debug.Log("Show interstitial now");
        //show interstitial here
        if (interstitial.IsLoaded())
        {
            Debug.Log("Show interstitial now as its loaded");
            interstitial.Show();
        }
        MenuController.hasPlayedInThisSession = true;
        interstitial.Destroy();
        SceneManager.LoadScene("FeedTheMonsters");
    }

    private void monsterReactsToItem(GameObject monster, GameObject item)
    {

        if (monster.tag == "BigMonster")
        {
            if (BigMonsterAnimator == null) BigMonsterAnimator = monster.GetComponent<Animator>();
            
            switch (item.tag)
            {
                case "Broccoli":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Bananas":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Carrot":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Cupcake":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Apple":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Eggplant":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Ball":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Pacifier":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Hat":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Glasses":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Book":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cellphone":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "CarToy":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Burger":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Keys":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "ToyTrain":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Shoe":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Sandwich":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Lemon":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Mitten":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Guitar":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "SoccerBall":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Donut":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "CoffeeMug":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "ChocolateBar":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Cake":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "BabyBottle":
                    playMonsterReaction(false, monster.tag);
                    break;
                default:
                    BigMonsterAnimator.SetTrigger("Eat");
                    bigMonsterSounds[0].Play();
                    break;
            }

        }
        else if (monster.tag == "MiddleMonster")
        {
            //if (MiddleMonsterAnimator == null) MiddleMonsterAnimator = monster.GetComponent<Animator>();
            switch (item.tag)
            {
                case "Bananas":
                    playMonsterReaction(true, monster.tag);
                    break;            
                case "Apple":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Broccoli":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Eggplant":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cupcake":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Carrot":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cellphone":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Glasses":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Book":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Ball":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "CarToy":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Hat":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Burger":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Keys":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Pacifier":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "ToyTrain":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Shoe":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Sandwich":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Lemon":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Mitten":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Guitar":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "SoccerBall":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Donut":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "CoffeeMug":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "ChocolateBar":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cake":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "BabyBottle":
                    playMonsterReaction(false, monster.tag);
                    break;
                default:
                    MiddleMonsterAnimator.SetTrigger("Eat");
                    middleMonsterSounds[0].Play();
                    break;
            }

        }
        else if (monster.tag == "LittleMonster")
        {
            //if (LittleMonsterAnimator == null) LittleMonsterAnimator = monster.GetComponent<Animator>();
            switch (item.tag)
            {
                case "Bananas":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Pacifier":
                    playMonsterReaction(true, monster.tag);
                    break;                 
                case "Apple":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Burger":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cupcake":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "CarToy":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Book":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Eggplant":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Glasses":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Carrot":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Broccoli":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Hat":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Cellphone":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Ball":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Keys":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "ToyTrain":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Shoe":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Sandwich":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Lemon":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Mitten":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Guitar":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "SoccerBall":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "Donut":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "CoffeeMug":
                    playMonsterReaction(false, monster.tag);
                    break;
                case "ChocolateBar":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "Cake":
                    playMonsterReaction(true, monster.tag);
                    break;
                case "BabyBottle":
                    playMonsterReaction(true, monster.tag);
                    break;
                default:
                    LittleMonsterAnimator.SetTrigger("Eat");
                    littleMonsterSounds[0].Play();
                    break;
            }
        }
        else
        {

        }
       
    }

    void playMonsterReaction(bool happy, string monsterTag)
    {
        if (monsterTag == "LittleMonster")
        {
            if (happy)
            {
                LittleMonsterAnimator.SetTrigger("Pleased");
                littleMonsterSounds[1].Play();
            }
            else
            {
                LittleMonsterAnimator.SetTrigger("Disgusted");
                littleMonsterSounds[2].Play();
            }
        }
        if (monsterTag == "MiddleMonster")
        {
            if (happy)
            {
                MiddleMonsterAnimator.SetTrigger("Pleased");
                middleMonsterSounds[1].Play();
            }
            else
            {
                MiddleMonsterAnimator.SetTrigger("Disgusted");
                middleMonsterSounds[2].Play();
            }
        }
        if (monsterTag == "BigMonster")
        {
            if (happy)
            {
                BigMonsterAnimator.SetTrigger("Pleased");
                bigMonsterSounds[1].Play();
            }
            else
            {
                BigMonsterAnimator.SetTrigger("Disgusted");
                bigMonsterSounds[2].Play();
            }
        }
    }

  
    IEnumerator playFinalShow()
    {
        //print(Time.time);
        yield return new WaitForSeconds(3);
        //print(Time.time);
        if (LittleMonsterAnimator != null && MiddleMonsterAnimator != null && BigMonsterAnimator != null && !finalDancePlaying) //all animators should be ok at this point already
        {
            radioSounds[0].Stop();
            radioSounds[1].Play();
            timeWhenFinalDanceStarted = Time.time;
            LittleMonsterAnimator.SetTrigger("Dance");
            MiddleMonsterAnimator.SetTrigger("Dance");
            BigMonsterAnimator.SetTrigger("Dance");
            finalDancePlaying = true;
        }
    }

}
